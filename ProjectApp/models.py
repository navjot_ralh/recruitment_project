from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class register_recruiter(models.Model):
    user=models.OneToOneField(User,on_delete=models.CASCADE)
    profile_pic=models.ImageField(upload_to='images/%y/%m/%d',null=True,blank=True)
    company=models.TextField()
    contact=models.IntegerField()
    city=models.TextField()
    address=models.TextField()
    registered_on=models.DateTimeField(auto_now_add=True)
    changed_on=models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.user.username

class register_candidate(models.Model):
    GENDER=(('m','male'),('f','female'))
    user=models.OneToOneField(User,on_delete=models.CASCADE)
    first=models.TextField()
    last=models.TextField()
    profile_pic=models.ImageField(upload_to='images/%y/%m/%d',null=True,blank=True)
    contact=models.IntegerField()
    dob=models.CharField(max_length=100)
    gender=models.CharField(choices=GENDER,max_length=100)
    city=models.TextField()
    address=models.TextField()
    registered_on=models.DateTimeField(auto_now_add=True)
    changed_on=models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.first+' '+ self.last

class contact(models.Model):
    name=models.TextField()
    phone_no=models.IntegerField()
    email=models.EmailField()
    subject=models.TextField()
    message=models.TextField()

    def __str__(self):
        return self.name

class post_job(models.Model):
    ex=(
        ('0','Fresher'),
        ('0-1','0-1 years'),
        ('1-2','1-2 years'),
        ('2-3','2-3 years'),
        ('3-4','3-4 years'),
        ('4-5','4-5 years'),
        ('5-6','5-6 years'),
        ('6-7','6-7 years'),
        ('7-8','7-8 years'),
        ('8-9','8-9 years'),
        ('9-10','9-10 years'),
    )
    name = models.ForeignKey(register_recruiter,on_delete=models.CASCADE,null=True)
    position=models.TextField()
    image=models.ImageField(upload_to='images/%y/%m/%d',null=True,blank=True)
    salary=models.IntegerField()
    vacancies=models.IntegerField()
    available=models.BooleanField(default=True)
    requirements=models.TextField()
    hr_name=models.TextField()
    hr_contact=models.IntegerField()
    city=models.TextField()
    experience=models.CharField(choices=ex,max_length=5,null=True,blank=True)
    description=models.TextField()
    posted_on=models.DateTimeField(auto_now_add=True)
    updated_on=models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.position

class addQuestion(models.Model):
    CHOICES = (
    ('a','A'),
    ('b','B'),
    ('c','C'),
    ('d','D')
    )
    question = models.CharField(max_length=500)
    option1 = models.CharField(max_length = 250)
    option2 = models.CharField(max_length = 250)
    option3 = models.CharField(max_length = 250)
    option4 = models.CharField(max_length = 250)
    answer = models.CharField(max_length= 1, choices = CHOICES, blank=True)
    description = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True, blank = True)

    def __str__(self):
        return self.question

class apply(models.Model):
    ex=(
    ('0','Fresher'),
    ('0-1','0-1 years'),
    ('1-2','1-2 years'),
    ('2-3','2-3 years'),
    ('3-4','3-4 years'),
    ('4-5','4-5 years'),
    ('5-6','5-6 years'),
    ('6-7','6-7 years'),
    ('7-8','7-8 years'),
    ('8-9','8-9 years'),
    ('9-10','9-10 years'),
)
    candidate=models.ForeignKey(register_candidate,on_delete=models.CASCADE,null=True)
    job=models.ForeignKey(post_job,on_delete=models.CASCADE,null=True)
    recruiterid = models.ForeignKey(register_recruiter,on_delete=models.CASCADE,null=True)
    qualification=models.CharField(max_length=100)
    graduation_percentage=models.DecimalField(max_digits=5,decimal_places=2)
    passing_out_year=models.IntegerField()
    resume=models.FileField(upload_to='resume/%Y/%m/%d')
    experience=models.CharField(choices=ex,max_length=5)
    applied_on=models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.candidate.user.username